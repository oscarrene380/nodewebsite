/*
    constantes o valores clave que se utilizaran en diversas partes de la app
*/
module.exports = {
    database: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'database_links'
    }
};